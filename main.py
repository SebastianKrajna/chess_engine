import sys
from PySide2.QtGui import QFont
from PySide2.QtWidgets import QHBoxLayout, QLabel, QPushButton, QWidget, QApplication, QGridLayout
from PySide2.QtCore import Qt, Signal
from game_board import GameBoard

from svg_button import SvgButton

import chess

import logging

logging.basicConfig(level=logging.DEBUG)
class MainWindow(QWidget):    
    
    signal_exit = Signal()
    
    def __init__(self):
        super().__init__()
        
        self.setUi()
        
    def setUi(self):
        
        BUTTON_HEIGHT = 50
        BUTTON_WIDTH = 200
        BUTTON_WIDTH2 = 50
        
        self.setGeometry(100, 100, 450, 450)
        font = QFont("Arial", 20, QFont.Bold);
        
        layout = QGridLayout()
        
        label = QLabel("Play as", font=font)
        
        self.boardOrientation = chess.WHITE
        
        self.button_white_queen = SvgButton('Q')
        self.button_black_queen = SvgButton('q')
                
        self.button_white_queen.setFixedSize(BUTTON_WIDTH2, BUTTON_HEIGHT)  
        self.button_black_queen.setFixedSize(BUTTON_WIDTH2, BUTTON_HEIGHT)  
        
        self.button_white_queen.setCheckable(True)
        self.button_white_queen.setChecked(True)
        
        self.button_black_queen.setCheckable(True)
        self.button_white_queen.setChecked(False)
        
        self.button_white_queen.clicked.connect(lambda: self.orientationMethod("Q"))
        self.button_black_queen.clicked.connect(lambda: self.orientationMethod("q"))
        
        layout_choose = QHBoxLayout()
        layout_choose.setSpacing(0)
        layout_choose.addWidget(self.button_white_queen)
        layout_choose.addWidget(self.button_black_queen)
        
        self.button_start_game = QPushButton('Start game')
        self.button_start_game.clicked.connect(self.startGame)
        self.button_start_game.setFont(font)
        self.button_start_game.setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT)
        
        layout.addWidget(label, 0, 0, Qt.AlignCenter)
        layout.addLayout(layout_choose, 1, 0, Qt.AlignCenter)
        layout.addWidget(self.button_start_game, 2, 0, Qt.AlignCenter)
                
        self.setLayout(layout)
    
        
    def exitMethod(self):
        self.signal_exit.emit()
        self.close()
        
    def orientationMethod(self, piece):
        if piece == "q":
            self.button_white_queen.setChecked(False)
            self.button_black_queen.setChecked(True)
            self.boardOrientation = chess.BLACK
        elif piece == "Q":
            self.button_white_queen.setChecked(True)
            self.button_black_queen.setChecked(False)
            self.boardOrientation = chess.WHITE

    def startGame(self):
        self.hide()
        self.game_board = GameBoard(self.boardOrientation)
        self.game_board.signal_exit.connect(self.exitMethod)
        self.game_board.show()
    

if __name__ == '__main__':
        
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec_()