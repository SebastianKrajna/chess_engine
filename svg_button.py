from PySide2.QtSvg import QSvgWidget
from PySide2.QtWidgets import  QHBoxLayout, QPushButton

import logging
import chess
import chess.svg

class SvgButton(QPushButton):
    def __init__(self, piece_type):
        super(SvgButton, self).__init__()
        self.checked = False
        self.piece_type = piece_type
        
        self.widgetSvg = QSvgWidget()

        layout = QHBoxLayout()
        layout.addWidget(self.widgetSvg)
        self.setLayout(layout)
            
    def paintEvent(self, event) -> None:
        self.pieceSvg = chess.svg.piece(chess.Piece.from_symbol(self.piece_type)).encode("UTF-8")
        self.widgetSvg.load(self.pieceSvg)
        return super().paintEvent(event)
    
    def changePieceColor(self):
        if self.piece_type.isupper():
            self.piece_type = self.piece_type.lower()
        else:
            self.piece_type = self.piece_type.upper()
        logging.debug(f'Piece {self.piece_type} color has been changed')
        self.update()