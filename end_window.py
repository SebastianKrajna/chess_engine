from PySide2.QtGui import QFont
from PySide2.QtWidgets import  QLabel, QPushButton,  QWidget, QGridLayout
from PySide2.QtCore import Qt, Signal

import chess

import logging
 

logging.basicConfig(level=logging.DEBUG)

class EndWindow(QWidget):
    
    signal_rematch = Signal()
    signal_exit = Signal()
     
    def __init__(self, outcome):
        super().__init__()
        
        self.outcome = outcome
        self.setUi()
                
        
    def setUi(self):
        
        BUTTON_HEIGHT = 50
        BUTTON_WIDTH = 200
        
        self.setGeometry(100, 100, 450, 450)
        font = QFont("Arial", 20, QFont.Bold);
        
        label_winner = QLabel("")
        if(self.outcome.winner != None):
            color = "WHITE" if self.outcome.winner == chess.WHITE else "BLACK"        
            label_winner = QLabel(f"{color} WON!", font=font)
        else:
            label_winner = QLabel("DRAW!", font=font)
        
        label_result = QLabel(f"{self.outcome.result()}", font=font)
        
        self.button_rematch = QPushButton("Rematch", font=font)
        self.button_exit = QPushButton("Exit", font=font)
        
        self.button_rematch.setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT)
        self.button_exit.setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT)
        
        layout = QGridLayout()
        layout.addWidget(label_winner, 0, 0, Qt.AlignCenter)        
        layout.addWidget(label_result, 1, 0, Qt.AlignCenter)      
        layout.addWidget(self.button_rematch, 2, 0, Qt.AlignCenter)      
        layout.addWidget(self.button_exit, 3, 0, Qt.AlignCenter)
        
        self.setLayout(layout)
    
        self.button_rematch.clicked.connect(self.rematchMethod)
        self.button_exit.clicked.connect(self.exitMethod)    


    def rematchMethod(self):
        self.signal_rematch.emit()
        self.close()
        
    def exitMethod(self):
        self.signal_exit.emit()
        self.close()