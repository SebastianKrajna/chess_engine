from PySide2.QtGui import QFont
from PySide2.QtWidgets import QLabel, QWidget, QGridLayout
from PySide2.QtSvg import QSvgWidget
from PySide2.QtCore import QThread, Qt, Signal

import chess
import chess.svg

import logging

from dashboard import Dashboard
from end_window import EndWindow

from simple_engine import Search

import subprocess

logging.basicConfig(level=logging.DEBUG)



class SearchThread(QThread):
    
    signal_move = Signal(str)
    
    def __init__(self, board):
        super().__init__()

        self.board = board
        self.depth = 4
        self.duration = 30

    def setDepth(self, depth):
        old_depth = self.depth
        self.depth = depth
        logging.debug(f"Change depth from {old_depth} to {self.depth}")
        
    def setDuration(self, duration):
        old_duration= self.duration
        self.duration = duration
        logging.debug(f"Change duration from {old_duration} to {self.duration}")
    
    def run(self):                
        
        try:
            bestMove = chess.polyglot.MemoryMappedReader("bookfish.bin").weighted_choice(self.board).move
            logging.debug(f"Find best book move: {bestMove.uci()} ")
            self.signal_move.emit(bestMove.uci())
            
        except IndexError: 
            logging.debug("Start engine script")
            
            cmd_fen = f'-f {self.board.fen()}'
            logging.debug(f"Sent fen: {cmd_fen}")
            
            cmd_depth = f'-d {str(self.depth)}'
            logging.debug(f"Sent depth: {cmd_depth}")
            
            cmd_duration = f'-t {str(self.duration)}'
            logging.debug(f"Sent duration: {cmd_duration}")           
            
            cmd = ['mpiexec', 'python', 'simple_engine.py', cmd_depth, cmd_duration, cmd_fen]
            process = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            process.wait()
            logging.debug("End engine script")
            
            result = process.stdout.readline()
            logging.debug(f"Got result {result}")
            self.signal_move.emit(str(result, 'utf-8'))
        

class GameBoard(QWidget):
    
    signal_exit = Signal()
    
    def __init__(self, orientation):
        super().__init__()
        
        self.orientation = orientation
        self.color_player = orientation
        self.color_engine = not orientation
        self.color_move = chess.WHITE
        self.disable_pieces = False
        
        self.promote_piece = 'q'
 
        self.setUi()

        self.chessboard = chess.Board()
        self.piece_pressed_square_name = None
        self.piece_pressed_type = None
        self.piece_legal_moves = []
        
        self.search_thread = SearchThread(self.chessboard)
        
        self.setSignal()
        self.checkGameMove()
        
    def setUi(self):
        self.setGeometry(100, 100, 1050, 620)
        font = QFont("Arial", 20, QFont.Bold)
        
        self.widgetSvg = QSvgWidget()
        self.widgetSvg.setGeometry(10, 10, 610, 610)
        self.widgetSvg.setFixedSize(600,600)
                
        self.dashboard = Dashboard(self.orientation)
        
        self.label_move = QLabel("", font=font)
        self.changeLabelMove()
        
        layout_main = QGridLayout()
        layout_main.setColumnStretch(2, 1) 
        layout_main.addWidget(self.label_move, 0, 0, Qt.AlignCenter | Qt.AlignTop)
        layout_main.addWidget(self.widgetSvg, 1, 0, Qt.AlignLeft)
        layout_main.addWidget(self.dashboard, 1, 1, Qt.AlignCenter | Qt.AlignTop)
        self.setLayout(layout_main)
        
        
    def checkGameMove(self):
        if self.color_engine == self.color_move:
            self.search_thread.start()
            logging.debug(f'Engine move')
        else:
            logging.debug(f'Player move')


    def changeLabelMove(self):
        who = "Engine" if self.color_move == self.color_engine else "Player"
        color = "White" if self.color_move == chess.WHITE else "Black"
        text = f"Now is {who} {color} move"
        self.label_move.setText(text)


    def setSignal(self):
        self.dashboard.signal_rotate.connect(self.changeOrientation)
        self.dashboard.signal_promote.connect(self.changePromotePiece)
        self.search_thread.signal_move.connect(self.computerMove)
        self.dashboard.signal_depth.connect(self.setDepth)
        self.dashboard.signal_duration.connect(self.setDuration)
    
    
    def setDepth(self, depth):
        logging.debug(f"Got depth in setDepth {depth}")
        self.search_thread.setDepth(depth)
        
        
    def setDuration(self, duration):
        logging.debug(f"Got duration in setDuration {duration}")
        self.search_thread.setDuration(duration)
        
        
    def computerMove(self, move):
        self.chessboard.push(chess.Move.from_uci(move))
        self.checkGameOver()
        self.color_move = not self.color_move
        self.changeLabelMove()
        self.disable_pieces = False
        logging.debug(f"Computer move is {move}")
    
        
    def paintEvent(self, event):
        self.chessboardSvg = chess.svg.board(self.chessboard,
                                             lastmove=self.chessboard.move_stack[-1] if self.chessboard.move_stack else chess.Move.null(), 
                                             orientation=self.orientation,
                                             squares=self.piece_legal_moves).encode("UTF-8")
        self.widgetSvg.load(self.chessboardSvg)


    def mousePressEvent(self, event):
        if self.disable_pieces == False:
            p = event.pos()
            gp = self.mapToGlobal(p)
            rw = self.window().mapFromGlobal(gp)
            logging.debug(f"Relative mouse pos: {rw}")
            square_name = self.getSquareName(rw.toTuple()) # 'e2'
            pressed_square = chess.parse_square(square_name) # 12 = chess.E2
            pressed_piece = self.chessboard.piece_at(pressed_square) # Piece.from_symbol('P') as PAWN
            
            if pressed_square in self.piece_legal_moves:
                self.piece_legal_moves.clear()
                move = f"{self.piece_pressed_square_name}{square_name}"
                if '7' in self.piece_pressed_square_name and '8' in square_name and self.piece_pressed_type == chess.PAWN:
                    move += self.promote_piece  
                elif '2' in self.piece_pressed_square_name and '1' in square_name and self.piece_pressed_type == chess.PAWN :
                    move += self.promote_piece
                self.chessboard.push_uci(move)
                self.checkGameOver()
                self.color_move = not self.color_move
                self.disable_pieces = True
                self.changeLabelMove()
                self.checkGameMove()
                
                
            elif None == pressed_piece or self.chessboard.turn != pressed_piece.color:
                self.piece_legal_moves.clear()
        
            elif self.chessboard.turn == pressed_piece.color:

                all_legal_moves = self.chessboard.legal_moves
                pressed_piece_legal_moves = []
                for m in chess.SQUARES:
                    if chess.Move(pressed_square, m) in all_legal_moves:
                        pressed_piece_legal_moves.append(m)
                
                if pressed_piece.piece_type == chess.PAWN:
                    if pressed_piece.color == chess.WHITE and '7' in square_name or \
                        pressed_piece.color == chess.BLACK and '2' in square_name:
                        for m in chess.FILE_NAMES:
                            if chess.Move.from_uci(f'{square_name}{m}1q') in all_legal_moves:
                                pressed_piece_legal_moves.append(chess.parse_square(f'{m}1'))
                            if chess.Move.from_uci(f'{square_name}{m}8q') in all_legal_moves:
                                pressed_piece_legal_moves.append(chess.parse_square(f'{m}8'))
                                    
                logging.debug(f"Piece at square {square_name} can move to {pressed_piece_legal_moves}")
                
                self.piece_legal_moves = chess.SquareSet(pressed_piece_legal_moves)
                self.piece_pressed_square_name = square_name
                self.piece_pressed_type = pressed_piece.piece_type

        
    def getSquareName(self, rw):        
        y = int((rw[0] - 34)/69)
        x = int((rw[1] - 71)/69)
        square_name = ""
        if self.orientation == chess.BLACK:
            square_name = chess.square_name(chess.square(7-y,x))
        else:
            square_name = chess.square_name(chess.square(y,7-x))
        logging.debug(f'{square_name}')
        return square_name
        
        
    def changeOrientation(self):
        self.orientation = chess.BLACK if (self.orientation == chess.WHITE) else chess.WHITE
        logging.debug(f'Rotate board: {"white" if self.orientation == 1 else "black"}')
        self.dashboard.setOrientation()


    def changePromotePiece(self, piece):
        self.promote_piece = piece
        logging.debug(f"Promote piece change to {self.promote_piece}")

    
    def checkGameOver(self):
        if(self.chessboard.is_game_over()):
            outcome = self.chessboard.outcome()
            self.end_window = EndWindow(outcome)
            self.end_window.signal_exit.connect(self.exitMethod)
            self.end_window.signal_rematch.connect(self.rematchMethod)
            self.end_window.show()
            
    def exitMethod(self):
        self.signal_exit.emit()
        self.close()
    
    def rematchMethod(self):
        self.changeOrientation()
        self.chessboard.reset()
        self.color_player = not self.color_player
        self.color_engine = not self.color_engine
        self.color_move = chess.WHITE
        if self.color_move == self.color_player:
            self.disable_pieces = False
        else:
            self.disable_pieces = True

        self.checkGameMove()
        