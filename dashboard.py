from PySide2.QtGui import QFont
from PySide2.QtWidgets import  QHBoxLayout, QLabel, QLineEdit, QWidget, QPushButton, QGridLayout
from PySide2.QtCore import Signal
from PySide2.QtCore import Qt

import logging

import chess

from svg_button import SvgButton
    

class Dashboard(QWidget):
    
    signal_rotate = Signal()
    signal_promote = Signal(str)
    signal_depth = Signal(int)
    signal_duration = Signal(int) 
    
    def __init__(self, orientation):
        super(Dashboard, self).__init__()
                
        BUTTON_HEIGHT = 50
        BUTTON_WIDTH = 200
        BUTTON_WIDTH2 = 50
        BUTTON_WIDTH3 = 90        
        font = QFont("Arial", 15, QFont.Bold);
            
        self.label_depth = QLabel("Depth", font=font)
        self.label_duration = QLabel("Duration", font=font)
            
        self.button_rotate = QPushButton('Obróć')
        self.button_rotate.setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT)
        
        self.button_queen  = SvgButton('Q' if orientation == chess.WHITE else 'q')
        self.button_rook   = SvgButton('R' if orientation == chess.WHITE else 'r')
        self.button_knight = SvgButton('N' if orientation == chess.WHITE else 'n')
        self.button_bishop = SvgButton('B' if orientation == chess.WHITE else 'b')
        
        self.button_queen.setCheckable(True)
        self.button_rook.setCheckable(True)
        self.button_knight.setCheckable(True)
        self.button_bishop.setCheckable(True)
        self.button_queen.setChecked(True)
                
        self.button_queen.setFixedSize(BUTTON_WIDTH2, BUTTON_HEIGHT)  
        self.button_rook.setFixedSize(BUTTON_WIDTH2, BUTTON_HEIGHT)  
        self.button_knight.setFixedSize(BUTTON_WIDTH2, BUTTON_HEIGHT)  
        self.button_bishop.setFixedSize(BUTTON_WIDTH2, BUTTON_HEIGHT)  
        
        layout_promote = QHBoxLayout()
        layout_promote.setSpacing(0)
        layout_promote.addWidget(self.button_queen)
        layout_promote.addWidget(self.button_rook)
        layout_promote.addWidget(self.button_knight)
        layout_promote.addWidget(self.button_bishop)
        
        self.edit_depth  = QLineEdit('4')
        self.minus_depth = QPushButton('-')
        self.plus_depth  = QPushButton('+')
        
        self.edit_depth.setAlignment(Qt.AlignCenter) 
        self.edit_depth.setFixedSize(BUTTON_WIDTH3, BUTTON_HEIGHT)
        self.minus_depth.setFixedSize(BUTTON_WIDTH2, BUTTON_HEIGHT)
        self.plus_depth.setFixedSize(BUTTON_WIDTH2, BUTTON_HEIGHT)
        
        depthLayout = QHBoxLayout()
        depthLayout.addWidget(self.minus_depth)
        depthLayout.addWidget(self.edit_depth)
        depthLayout.addWidget(self.plus_depth)
        
        self.edit_duration  = QLineEdit('30')
        self.minus_duration = QPushButton('-')
        self.plus_duration  = QPushButton('+')
        
        self.edit_duration.setAlignment(Qt.AlignCenter) 
        self.edit_duration.setFixedSize(BUTTON_WIDTH3, BUTTON_HEIGHT)
        self.minus_duration.setFixedSize(BUTTON_WIDTH2, BUTTON_HEIGHT)
        self.plus_duration.setFixedSize(BUTTON_WIDTH2, BUTTON_HEIGHT)
        
        durationLayout = QHBoxLayout()
        durationLayout.addWidget(self.minus_duration)
        durationLayout.addWidget(self.edit_duration)
        durationLayout.addWidget(self.plus_duration)
        
        layout = QGridLayout()
        layout.addWidget(self.button_rotate, 0, 0, Qt.AlignTop)
        layout.addLayout(layout_promote, 1, 0, Qt.AlignTop)
        layout.addWidget(self.label_depth, 2, 0, Qt.AlignTop | Qt.AlignCenter)
        layout.addLayout(depthLayout, 3, 0, Qt.AlignTop)
        layout.addWidget(self.label_duration, 4, 0, Qt.AlignTop | Qt.AlignCenter)
        layout.addLayout(durationLayout, 5, 0, Qt.AlignTop)
        
        self.setLayout(layout)
        
        self.button_rotate.clicked.connect(self.rotateMethod)
        self.button_queen.clicked.connect(lambda: self.promoteMethod("q"))
        self.button_rook.clicked.connect(lambda: self.promoteMethod("r"))
        self.button_knight.clicked.connect(lambda: self.promoteMethod("n"))
        self.button_bishop.clicked.connect(lambda: self.promoteMethod("b"))
        self.minus_depth.clicked.connect(lambda: self.changeDepth(-1))
        self.plus_depth.clicked.connect(lambda: self.changeDepth(1))
        self.minus_duration.clicked.connect(lambda: self.changeDuration(-1))
        self.plus_duration.clicked.connect(lambda: self.changeDuration(1))
        self.edit_depth.textChanged.connect(lambda: self.changeDepth(0))
        self.edit_duration.textChanged.connect(lambda: self.changeDuration(0))
        
    def changeDepth(self, val=0):
        depth = int(self.edit_depth.text()) + val
        if depth < 1: depth = 1
        logging.debug(f'Changed depth to: {depth}')
        self.edit_depth.setText(str(depth))
        self.signal_depth.emit(depth)

    def changeDuration(self, val=0):
        duration = int(self.edit_duration.text()) + val
        if duration < 1: duration = 1
        logging.debug(f'Changed duration to: {duration}')
        self.edit_duration.setText(str(duration))
        self.signal_duration.emit(duration)

    def rotateMethod(self):
        self.signal_rotate.emit()

    def promoteMethod(self, piece):
        if piece == "q":
            self.button_queen.setChecked(True)
            self.button_rook.setChecked(False)
            self.button_knight.setChecked(False)
            self.button_bishop.setChecked(False)
        elif piece == "r":
            self.button_queen.setChecked(False)
            self.button_rook.setChecked(True)
            self.button_knight.setChecked(False)
            self.button_bishop.setChecked(False)
        elif piece == "n":
            self.button_queen.setChecked(False)
            self.button_rook.setChecked(False)
            self.button_knight.setChecked(True)
            self.button_bishop.setChecked(False)
        elif piece == "b":
            self.button_queen.setChecked(False)
            self.button_rook.setChecked(False)
            self.button_knight.setChecked(False)
            self.button_bishop.setChecked(True)
        
        logging.debug(f'Queen state: {self.button_queen.isChecked()}')
        logging.debug(f'Rook state: {self.button_rook.isChecked()}')
        logging.debug(f'Knight state: {self.button_knight.isChecked()}')
        logging.debug(f'Bishop state: {self.button_bishop.isChecked()}')
            
        self.signal_promote.emit(piece)
    
    def setOrientation(self):
        self.button_queen.changePieceColor()
        self.button_rook.changePieceColor()
        self.button_knight.changePieceColor()
        self.button_bishop.changePieceColor()

        