import chess
import chess.svg
import chess.polyglot

from collections import namedtuple
import sys
import time

import math
import numpy as np

import logging

from constants import *
from mpi4py import MPI

comm = MPI.COMM_WORLD
num_of_proc = comm.Get_size()
proc_num = comm.Get_rank()

from argparse import ArgumentParser

logging.basicConfig(level=logging.DEBUG)

TTtuple = namedtuple('TTEntry', 'zobrist move depth score type')

class SearchStoppedException(Exception):
    pass

class Search():

    def __init__(self, max_depth=math.inf, time_limit=math.inf):

        self.board = None
        self.checking_moves = None
        self.max_depth = max_depth
        self.time_limit = time_limit

        self.start_time = None
        self.node_count = 0
        self.gamephase = 0


    def evaluate_board(self):

        wp = len(self.board.pieces(chess.PAWN, chess.WHITE))
        bp = len(self.board.pieces(chess.PAWN, chess.BLACK))
        wn = len(self.board.pieces(chess.KNIGHT, chess.WHITE))
        bn = len(self.board.pieces(chess.KNIGHT, chess.BLACK))
        wb = len(self.board.pieces(chess.BISHOP, chess.WHITE))
        bb = len(self.board.pieces(chess.BISHOP, chess.BLACK))
        wr = len(self.board.pieces(chess.ROOK, chess.WHITE))
        br = len(self.board.pieces(chess.ROOK, chess.BLACK))
        wq = len(self.board.pieces(chess.QUEEN, chess.WHITE))
        bq = len(self.board.pieces(chess.QUEEN, chess.BLACK))

        material = 0

        # Decide gamephase - (0 is early game, 1 is late)
        if self.gamephase == 0:
            materialcout =  (wp+bp) * materialWeights[0][1]
            materialcout += (wn+bn) * materialWeights[0][2]
            materialcout += (wb+bb) * materialWeights[0][3]
            materialcout += (wr+br) * materialWeights[0][4]
            materialcout += (wq+bq) * materialWeights[0][5]
            if materialcout <= 15258:
                self.gamephase = 1

        material =  (wp-bp) * materialWeights[self.gamephase][1]
        material += (wn-bn) * materialWeights[self.gamephase][2]
        material += (wb-bb) * materialWeights[self.gamephase][3]
        material += (wr-br) * materialWeights[self.gamephase][3]
        material += (wq-bq) * materialWeights[self.gamephase][4]

        pawnsq = sum([pawntable[self.gamephase][i] for i in self.board.pieces(chess.PAWN, chess.WHITE)])
        pawnsq= pawnsq + sum([-pawntable[self.gamephase][chess.square_mirror(i)]
                                        for i in self.board.pieces(chess.PAWN, chess.BLACK)])
        knightsq = sum([knightstable[self.gamephase][i] for i in self.board.pieces(chess.KNIGHT, chess.WHITE)])
        knightsq = knightsq + sum([-knightstable[self.gamephase][chess.square_mirror(i)]
                                        for i in self.board.pieces(chess.KNIGHT, chess.BLACK)])
        bishopsq= sum([bishopstable[self.gamephase][i] for i in self.board.pieces(chess.BISHOP, chess.WHITE)])
        bishopsq= bishopsq + sum([-bishopstable[self.gamephase][chess.square_mirror(i)]
                                        for i in self.board.pieces(chess.BISHOP, chess.BLACK)])
        rooksq = sum([rookstable[self.gamephase][i] for i in self.board.pieces(chess.ROOK, chess.WHITE)])
        rooksq = rooksq + sum([-rookstable[self.gamephase][chess.square_mirror(i)]
                                        for i in self.board.pieces(chess.ROOK, chess.BLACK)])
        queensq = sum([queenstable[self.gamephase][i] for i in self.board.pieces(chess.QUEEN, chess.WHITE)])
        queensq = queensq + sum([-queenstable[self.gamephase][chess.square_mirror(i)]
                                        for i in self.board.pieces(chess.QUEEN, chess.BLACK)])
        kingsq = sum([kingstable[self.gamephase][i] for i in self.board.pieces(chess.KING, chess.WHITE)])
        kingsq = kingsq + sum([-kingstable[self.gamephase][chess.square_mirror(i)]
                                        for i in self.board.pieces(chess.KING, chess.BLACK)])

        eval = material + pawnsq + knightsq + bishopsq + rooksq + queensq + kingsq
        if self.board.turn:
            return eval
        else:
            return -eval

 
    def pvSearchParallel(self, alpha, beta, depth):
        
        bestmove = chess.Move.null()
        bestscore = -999999
        bSearchPv = True if alpha != beta - 1 else False
        
        if self.node_count & 100:
            if time.time() - self.start_time >= self.time_limit:
                raise SearchStoppedException
        
        self.checking_moves = sorted(self.checking_moves, key=lambda l:l[1], reverse=True)
        
        for i, (move, sc) in enumerate(self.checking_moves):
            self.board.push(move)   
            if(bSearchPv):
                score = -self.pvSearch(-beta, -alpha, depth - 1)
            else:
                score = -self.pvSearch(-alpha-1, -alpha, depth - 1)
                if ( score > alpha and score < beta):
                    score = -self.pvSearch(-beta, -alpha, depth - 1)
            self.board.pop()
                  
            self.checking_moves[i][1] = score;      
                    
        return bestmove, bestscore
 
    
    def pvSearch(self, alpha, beta, depth):
        self.node_count += 1
                
        if self.board.can_claim_draw():
            return 0

        if depth <= 0:
            return self.quiesce(alpha, beta)
        
        if self.node_count & 100:
            if time.time() - self.start_time >= self.time_limit:
                raise SearchStoppedException
        
        bestscore = -999998
        bSearchPv = True if alpha != beta - 1 else False
        
        
        if not self.board.is_check() and not bSearchPv:
            depth_reduction = 2
            self.board.push(chess.Move.null())
            null_score = -self.pvSearch(-beta, -beta + 1, depth - depth_reduction - 1)
            self.board.pop()

            if null_score >= beta:
                return null_score
            if null_score <= -999999:
                depth += 1
        
        
        for move in self.board.legal_moves:
            self.board.push(move)   
            if(bSearchPv):
                score = -self.pvSearch(-beta, -alpha, depth - 1)
            else:
                score = -self.pvSearch(-alpha-1, -alpha, depth - 1)
                if ( score > alpha and score < beta):
                    score = -self.pvSearch(-beta, -alpha, depth - 1)
            self.board.pop()
            
            if score > bestscore:
                if score > alpha:
                    if score >= beta:
                        return score
                    alpha = score
                    bSearchPv = False 
                bestscore = score
        
        return bestscore

    
    def quiesce(self, alpha, beta):
        self.node_count += 1
        
        if self.board.can_claim_draw():
            return 0
        
        if self.board.is_checkmate():
            return -999999
        
        if self.node_count & 100:
            if time.time() - self.start_time >= self.time_limit:
                raise SearchStoppedException
        
        static_eval = self.evaluate_board()
        
        if static_eval > alpha:
            if static_eval >= beta:
                return static_eval
            alpha = static_eval
        best_score = static_eval

        for move in self.board.legal_moves:
            if self.board.is_capture(move):
                self.board.push(move)        
                score = -self.quiesce(-beta, -alpha)
                self.board.pop()

                if score > best_score:
                    if score > alpha:
                        if score >= beta:
                            return score
                        alpha = score
                    best_score = score 
        return best_score


    def selectmove(self, board, moves):
        self.board = board
        self.checking_moves = [[chess.Move.from_uci(move), -999999] for move in moves]
        self.node_count = 0
        self.start_time = time.time()
                                        
        depth = 0
        bestMove = chess.Move.null()
        bestscore = -999999
        alpha = -100000
        beta = 100000
        
        while depth < self.max_depth and (time.time() - self.start_time) < self.time_limit:
            depth += 1
            try:
                self.pvSearchParallel(-beta, -alpha, depth)
            except SearchStoppedException:
                break
            except:
                break
            
            
            self.checking_moves = sorted(self.checking_moves, key=lambda l:l[1], reverse=True)
            bestMove, bestscore = self.checking_moves[0]
            logging.debug(f"-------------------------------------------------------------------------")
            logging.debug(f"Proc num: {proc_num}")
            logging.debug(f"Find best move in: {time.time() - self.start_time} move: {bestMove.uci()}")
            logging.debug(f"Depth: {depth}, searched node: {self.node_count}")
            
        bestMove, bestscore = self.checking_moves[0]
        return [bestMove.uci(), int(bestscore)]


def prepareSearch(board, moves, depth, duration):
    search = Search(max_depth=int(depth), time_limit=int(duration))
    copyboard = board.copy()
    move = search.selectmove(copyboard, moves)
    return move


if __name__ == "__main__":
    
    parser = ArgumentParser()
    parser.add_argument("-f", "--fen", dest="fen",)
    parser.add_argument("-d", "--depth", dest="depth",)
    parser.add_argument("-t", "--duration", dest="duration",)
    args = parser.parse_args()
    
    if proc_num == 0:
        logging.debug(f"Got fen {args.fen}")
        logging.debug(f"Got depth {args.depth}")
        logging.debug(f"Got duration {args.duration}")
    board = chess.Board(args.fen)

    if proc_num == 0:
        data = [move.uci() for move in list(board.legal_moves)]
        size = int(np.ceil(len(data)/num_of_proc))
        data = np.resize(data, (num_of_proc, size))   
    else:
        data = None
     
    moves_per_proc = comm.scatter(data, root=0)
    
    logging.debug(f"{proc_num} recvbuf received: {moves_per_proc}")
    
    sendvbuf = prepareSearch(board, moves_per_proc, args.depth, args.duration)
    
    logging.debug(f"{proc_num} sendvbuf: {sendvbuf}")
    
    result = comm.gather(sendvbuf, root=0)
    
    if proc_num == 0:
        
        result = sorted(result, key=lambda l:l[1], reverse=True)
        logging.debug(f"result: {result}")

        sys.stdout.write(result[0][0])


